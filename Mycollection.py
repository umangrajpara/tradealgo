from Pinbar import Pinbar
from HangingMan import HangingMan
from BearEngulf import BearEngulf
from BullEngulf import BullEngulf
from newStrat import newStrat
import UniversalVar



class Strategy:
    """Common base class for all strategies"""

    def __init__(self, data, csvreader, current_day, Days, Target, Stoploss, Entry_LowerLimit, Entry_UpperLimit, filename):
        self.data = data
        self.stock_name = filename
        self.csvreader = csvreader
        self.current_day = current_day
        self.PevDlypercent = data[current_day][14]
        self.volume = float(data[current_day][10])
        self.date = data[current_day][2]
        self.nextvolume = float(data[current_day + 1][10])
        self.two_day_ago_open = float(data[current_day - 2][4])
        self.two_day_ago_high = float(data[current_day - 2][5])
        self.two_day_ago_low = float(data[current_day - 2][6])
        self.two_day_ago_close = float(data[current_day - 2][8])
        self.one_day_ago_open = float(data[current_day - 1][4])
        self.one_day_ago_high = float(data[current_day - 1][5])
        self.one_day_ago_low = float(data[current_day - 1][6])
        self.one_day_ago_close = float(data[current_day - 1][8])
        self.open = float(data[current_day][4])
        self.high = float(data[current_day][5])
        self.low = float(data[current_day][6])
        self.close = float(data[current_day][8])
        self.nextopen = float(data[current_day + 1][4])
        self.nexthigh = float(data[current_day + 1][5])
        self.nextlow = float(data[current_day + 1][6])
        self.nextclose = float(data[current_day + 1][8])
        self.Target = Target
        self.Days = Days
        self.Stoploss = Stoploss
        self.Entry_LowerLimit = 1 + (0.01 * Entry_LowerLimit)
        self.Entry_UpperLimit = 1 + (0.01 * Entry_UpperLimit)
        if UniversalVar.EntryAt == "Next Open":
            self.EntryPrice = self.nextopen
            RdButtonSelected(self)

        if UniversalVar.EntryAt == "Close":
            self.EntryPrice = self.close
            RdButtonSelected(self)

    # trading strategies
    def execute(self):

        if (UniversalVar.strategy == "Pinbar"):
            Pinbar(self)
        if (UniversalVar.strategy == "BearEngulf"):
            BearEngulf(self)
        if (UniversalVar.strategy == "BullEngulf"):
            BullEngulf(self)
        if (UniversalVar.strategy == "HangingMan"):
            HangingMan(self)
        if (UniversalVar.strategy == "newStrat"):
            newStrat(self)
