import csv, sys
import pandas as pd
import UniversalVar

from PyQt5 import QtWidgets
from Mycollection import Strategy
from ReportWindow import Ui_MainWindow_3
import pdfkit
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def GetReport():
    Days = 30
    Target = 1
    Stoploss = 2
    Entry_LowerLimit = -1
    Entry_UpperLimit = 0
    UniversalVar.strategy = "newStrat"
    UniversalVar.EntryAt = "nextopen"

    UniversalVar.stockcount = 0
    UniversalVar.tradecount = 0
    UniversalVar.points = 0
    UniversalVar.totallose = 0
    UniversalVar.totalwin = 0
    UniversalVar.count = 0
    UniversalVar.RESULT = []

    for k in UniversalVar.stocks:

        filename = k + ".csv"
        UniversalVar.stockcount += 1
        UniversalVar.RESULT.insert(UniversalVar.count, [k.upper(), "", "", "", "", ""])
        UniversalVar.count += 1

        with open('/home/umang/Backtest/stocks/' + filename, 'r') as csvfile:

            csvreader = csv.reader(csvfile)
            data = list(csvreader)
            for current_day in range(5, csvreader.line_num - 1):

                stock = Strategy(data, csvreader, current_day, Days, Target, Stoploss,
                                 Entry_LowerLimit, Entry_UpperLimit,filename)

                stock.execute()

    resultdf = pd.DataFrame(UniversalVar.RESULT,
                                    columns=['Stock', 'Date', 'PrevDelivery%', 'Above R4', 'Below S4',
                                             'P/L'])

    fig, ax = plt.subplots(figsize=(12, 4))
    ax.axis('tight')
    ax.axis('off')
    the_table = ax.table(cellText=resultdf.values, colLabels=resultdf.columns, loc='center')

    # pp = PdfPages("report.pdf")
    # pp.savefig(fig, bbox_inches='tight')
    # pp.close()

    app = QtWidgets.QApplication(sys.argv)
    rw = Ui_MainWindow_3()
    MainWindow_3 = QtWidgets.QMainWindow()
    rw.setupUi(MainWindow_3, resultdf)
    MainWindow_3.show()
    sys.exit(app.exec_())


GetReport()
