import UniversalVar


# BUYING STOCKS

def newStrat(self):
    # print(self.stock_name, self.date, self.one_day_ago_high, self.one_day_ago_low, self.one_day_ago_close)
    # print(float(self.PevDlypercent.strip()))
    R3 = ((self.one_day_ago_high - self.one_day_ago_low) * 0.275) + self.one_day_ago_close
    S3 = self.one_day_ago_close - ((self.one_day_ago_high - self.one_day_ago_low) * 0.275)
    R4 = ((self.one_day_ago_high - self.one_day_ago_low) * 0.55) + self.one_day_ago_close
    S4 = self.one_day_ago_close - ((self.one_day_ago_high - self.one_day_ago_low) * 0.55)
    one_d_ago_R3 = ((self.two_day_ago_high - self.two_day_ago_low) * 0.275) + self.two_day_ago_close
    one_d_ago_S3 = self.two_day_ago_close - ((self.two_day_ago_high - self.two_day_ago_low) * 0.275)

    if ((R3 < one_d_ago_R3) & (S3 > one_d_ago_S3) &(float(self.PevDlypercent.strip()) > 30)):

        upward_diff = (self.high - R3) * 100 / R3
        downward_diff = (S3 - self.low) * 100 / S3

        if ((upward_diff > 1) & (downward_diff < 0)) | ((upward_diff > 1) & (downward_diff > 1)):
            P_L = "Buy"
            UniversalVar.totalwin += 1

            UniversalVar.RESULT.insert(UniversalVar.count,
                                       ["", self.date, self.PevDlypercent, "{:.2f}".format(upward_diff),
                                        "{:.2f}".format(downward_diff), P_L])

        elif ((upward_diff < 0) & (downward_diff > 1)):
            P_L = " "
            # UniversalVar.totalwin += 1

            UniversalVar.RESULT.insert(UniversalVar.count,
                                       ["", self.date, self.PevDlypercent, "{:.2f}".format(upward_diff),
                                        "{:.2f}".format(downward_diff), P_L])
        elif (0<upward_diff < 1):
            P_L = "Loss"
            UniversalVar.totallose += 1

            UniversalVar.RESULT.insert(UniversalVar.count,
                                       ["", self.date, self.PevDlypercent, "{:.2f}".format(upward_diff),
                                        "{:.2f}".format(downward_diff), P_L])


        UniversalVar.count += 1
        UniversalVar.tradecount += 1
